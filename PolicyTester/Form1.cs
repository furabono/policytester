﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolicyTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            policySaveButton.Click += PolicySaveButton_Click;

            loadButton.Click += LoadButton_Click;
            addButton.Click += AddButton_Click;
            executeButton.Click += ExecuteButton_Click;

            DataGridViewTextBoxColumn eval = new DataGridViewTextBoxColumn();
            eval.Name = "EVALUATION";
            //eval.ValueType = typeof(double);
            dataGridView.Columns.Insert(0, eval);

            progressBar.Visible = false;

            pfmf = new GUI.PolicyFileManageForm();

            Debug.Debug.UseDebug = true;
            Debug.Debug.ShowDebugWindow();
        }

        private void PolicySaveButton_Click(object sender, EventArgs e)
        {
            string name;
            if (policy != null) { name = policy.Name; }
            else { name = "Untitled"; }

            if (pfmf.ShowDialog(name) == DialogResult.OK)
            {
                var variables = (from Control control in variableFlowLayoutPanel.Controls
                                 where control is GUI.PolicyVariable
                                 let pv = (GUI.PolicyVariable)control
                                 select new KeyValuePair<string, float>(pv.GetName(), pv.GetValue())).ToArray();
                Utility.JsonReader.WritePolicyData(pfmf.FullPath, pfmf.FileName, variables, scriptTextBox.Text);
            }
        }

        private async void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Application.StartupPath;
            ofd.Filter = "Json 파일 (*.json)|*.json";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                SetButtonsEnable(false, true);
                citizen = await Utility.JsonReader.ReadCitizenDataAsync(ofd.FileName);
                dataGridView.DataSource = citizen;
                SetButtonsEnable(true, false);
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            variableFlowLayoutPanel.Controls.Remove((Button)sender);
            string name = string.Format("Var{0}", variableFlowLayoutPanel.Controls.Count);
            GUI.PolicyVariable pv = new GUI.PolicyVariable(name);
            pv.RemoveButtonClick += RemoveButton_Click;
            variableFlowLayoutPanel.Controls.Add(pv);
            variableFlowLayoutPanel.Controls.Add((Button)sender);
            pv.EditMode();
        }

        private async void ExecuteButton_Click(object sender, EventArgs e)
        {
            if(citizen == null) { return; }

            SetButtonsEnable(false, true);

            Debug.Debug.PrintLn(DateTime.Now);
            KeyValuePair<string, float>[] variables = (from Control c in variableFlowLayoutPanel.Controls
                                                       where c is GUI.PolicyVariable
                                                       let pv = (GUI.PolicyVariable)c
                                                       select new KeyValuePair<string, float>(pv.GetName(), pv.GetValue())).ToArray();
            Debug.Debug.PrintLn(DateTime.Now);
            float[] result = await Utility.Simulator.SimulateParallelAsync(citizen, variables, "result = " + scriptTextBox.Text);
            Debug.Debug.PrintLn(DateTime.Now);

            if (!dataGridView.Columns.Contains("EVALUATION"))
            {
                DataGridViewTextBoxColumn eval = new DataGridViewTextBoxColumn();
                eval.Name = "EVALUATION";
                //eval.ValueType = typeof(double);
                dataGridView.Columns.Insert(0, eval);
            }
            Debug.Debug.PrintLn(DateTime.Now);
            dataGridView.SelectAll();
            for (int i = 0; i < result.Length; ++i)
            {
                dataGridView[0, i].Value = result[i];
            }
            Debug.Debug.PrintLn(DateTime.Now);

            SetButtonsEnable(true, false);
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            variableFlowLayoutPanel.Controls.Remove(((Button)sender).Parent);
        }

        private void SetButtonsEnable(bool enable, bool progress)
        {
            executeButton.Enabled = enable;
            loadButton.Enabled = enable;
            policyLoadButton.Enabled = enable;
            policySaveButton.Enabled = enable;
            progressBar.Visible = progress;
        }

        DataTable citizen;
        GUI.PolicyFileManageForm pfmf;
        Utility.Policy policy;
    }
}
