﻿namespace PolicyTester
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.variableGroupBox = new System.Windows.Forms.GroupBox();
            this.variableFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.addButton = new System.Windows.Forms.Button();
            this.scriptGroupBox = new System.Windows.Forms.GroupBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.buttonPanel = new System.Windows.Forms.Panel();
            this.loadButton = new System.Windows.Forms.Button();
            this.executeButton = new System.Windows.Forms.Button();
            this.policyMenuFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.policySaveButton = new System.Windows.Forms.Button();
            this.policyLoadButton = new System.Windows.Forms.Button();
            this.scriptTextBox = new System.Windows.Forms.TextBox();
            this.policyNewButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.variableGroupBox.SuspendLayout();
            this.variableFlowLayoutPanel.SuspendLayout();
            this.scriptGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.buttonPanel.SuspendLayout();
            this.policyMenuFlowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.progressBar, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.variableGroupBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.scriptGroupBox, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.dataGridView, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.buttonPanel, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.policyMenuFlowLayoutPanel, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(865, 531);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Right;
            this.progressBar.Location = new System.Drawing.Point(812, 513);
            this.progressBar.MarqueeAnimationSpeed = 50;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(50, 15);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 2;
            // 
            // variableGroupBox
            // 
            this.variableGroupBox.Controls.Add(this.variableFlowLayoutPanel);
            this.variableGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variableGroupBox.Location = new System.Drawing.Point(3, 3);
            this.variableGroupBox.Name = "variableGroupBox";
            this.variableGroupBox.Size = new System.Drawing.Size(859, 114);
            this.variableGroupBox.TabIndex = 0;
            this.variableGroupBox.TabStop = false;
            this.variableGroupBox.Text = "Variables";
            // 
            // variableFlowLayoutPanel
            // 
            this.variableFlowLayoutPanel.AutoScroll = true;
            this.variableFlowLayoutPanel.Controls.Add(this.addButton);
            this.variableFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variableFlowLayoutPanel.Location = new System.Drawing.Point(3, 17);
            this.variableFlowLayoutPanel.Name = "variableFlowLayoutPanel";
            this.variableFlowLayoutPanel.Size = new System.Drawing.Size(853, 94);
            this.variableFlowLayoutPanel.TabIndex = 0;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(3, 3);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(133, 52);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            // 
            // scriptGroupBox
            // 
            this.scriptGroupBox.Controls.Add(this.scriptTextBox);
            this.scriptGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptGroupBox.Location = new System.Drawing.Point(3, 153);
            this.scriptGroupBox.Name = "scriptGroupBox";
            this.scriptGroupBox.Size = new System.Drawing.Size(859, 44);
            this.scriptGroupBox.TabIndex = 1;
            this.scriptGroupBox.TabStop = false;
            this.scriptGroupBox.Text = "Script";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 233);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowTemplate.Height = 23;
            this.dataGridView.Size = new System.Drawing.Size(859, 274);
            this.dataGridView.TabIndex = 2;
            // 
            // buttonPanel
            // 
            this.buttonPanel.Controls.Add(this.loadButton);
            this.buttonPanel.Controls.Add(this.executeButton);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPanel.Location = new System.Drawing.Point(3, 203);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.Size = new System.Drawing.Size(859, 24);
            this.buttonPanel.TabIndex = 3;
            // 
            // loadButton
            // 
            this.loadButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.loadButton.Location = new System.Drawing.Point(0, 0);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 24);
            this.loadButton.TabIndex = 1;
            this.loadButton.Text = "Load Json";
            this.loadButton.UseVisualStyleBackColor = true;
            // 
            // executeButton
            // 
            this.executeButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.executeButton.Location = new System.Drawing.Point(784, 0);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(75, 24);
            this.executeButton.TabIndex = 0;
            this.executeButton.Text = "Execute";
            this.executeButton.UseVisualStyleBackColor = true;
            // 
            // policyMenuFlowLayoutPanel
            // 
            this.policyMenuFlowLayoutPanel.Controls.Add(this.policySaveButton);
            this.policyMenuFlowLayoutPanel.Controls.Add(this.policyLoadButton);
            this.policyMenuFlowLayoutPanel.Controls.Add(this.policyNewButton);
            this.policyMenuFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.policyMenuFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.policyMenuFlowLayoutPanel.Location = new System.Drawing.Point(3, 123);
            this.policyMenuFlowLayoutPanel.Name = "policyMenuFlowLayoutPanel";
            this.policyMenuFlowLayoutPanel.Size = new System.Drawing.Size(859, 24);
            this.policyMenuFlowLayoutPanel.TabIndex = 4;
            // 
            // policySaveButton
            // 
            this.policySaveButton.Location = new System.Drawing.Point(779, 0);
            this.policySaveButton.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.policySaveButton.Name = "policySaveButton";
            this.policySaveButton.Size = new System.Drawing.Size(75, 23);
            this.policySaveButton.TabIndex = 0;
            this.policySaveButton.Text = "Save";
            this.policySaveButton.UseVisualStyleBackColor = true;
            // 
            // policyLoadButton
            // 
            this.policyLoadButton.Location = new System.Drawing.Point(699, 0);
            this.policyLoadButton.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.policyLoadButton.Name = "policyLoadButton";
            this.policyLoadButton.Size = new System.Drawing.Size(75, 23);
            this.policyLoadButton.TabIndex = 1;
            this.policyLoadButton.Text = "Load";
            this.policyLoadButton.UseVisualStyleBackColor = true;
            // 
            // scriptTextBox
            // 
            this.scriptTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptTextBox.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.scriptTextBox.Location = new System.Drawing.Point(3, 17);
            this.scriptTextBox.Name = "scriptTextBox";
            this.scriptTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.scriptTextBox.Size = new System.Drawing.Size(853, 22);
            this.scriptTextBox.TabIndex = 0;
            this.scriptTextBox.WordWrap = false;
            // 
            // policyNewButton
            // 
            this.policyNewButton.Location = new System.Drawing.Point(574, 0);
            this.policyNewButton.Margin = new System.Windows.Forms.Padding(0, 0, 50, 0);
            this.policyNewButton.Name = "policyNewButton";
            this.policyNewButton.Size = new System.Drawing.Size(75, 23);
            this.policyNewButton.TabIndex = 2;
            this.policyNewButton.Text = "New";
            this.policyNewButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 531);
            this.Controls.Add(this.tableLayoutPanel);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel.ResumeLayout(false);
            this.variableGroupBox.ResumeLayout(false);
            this.variableFlowLayoutPanel.ResumeLayout(false);
            this.scriptGroupBox.ResumeLayout(false);
            this.scriptGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.buttonPanel.ResumeLayout(false);
            this.policyMenuFlowLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.GroupBox variableGroupBox;
        private System.Windows.Forms.GroupBox scriptGroupBox;
        private System.Windows.Forms.FlowLayoutPanel variableFlowLayoutPanel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel buttonPanel;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.FlowLayoutPanel policyMenuFlowLayoutPanel;
        private System.Windows.Forms.Button policySaveButton;
        private System.Windows.Forms.Button policyLoadButton;
        private System.Windows.Forms.TextBox scriptTextBox;
        private System.Windows.Forms.Button policyNewButton;
    }
}

