﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace PolicyTester.GUI
{
    public partial class PolicyFileManageForm : Form
    {
        public PolicyFileManageForm()
        {
            InitializeComponent();

            listBox.SelectedIndexChanged += ListBox_SelectedIndexChanged;
            listBox.MouseDoubleClick += ListBox_MouseDoubleClick;

            okButton.Click += OkButton_Click;
        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listBox = (ListBox)sender;

            this.textBox.Text = (string)listBox.SelectedValue;
        }

        private void ListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (ValidateName())
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (ValidateName())
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        public DialogResult ShowDialog(string filename = "")
        {
            string path = Application.StartupPath + @"\files";

            listBox.Items.Clear();
            textBox.Text = filename;

            DirectoryInfo di = new DirectoryInfo(path);
            if(!di.Exists) { di.Create(); }
            else
            {
                Regex regex = new Regex(@"([^\/:*?""<>|]+)\.([^\/:*?""<>|.]+)$");
                Match match;
                foreach (FileInfo fi in di.GetFiles())
                {
                    match = regex.Match(fi.Name);
                    if (match.Groups.Count != 3) { continue; }
                    if (match.Groups[2].Value.ToLower() != "json") { continue; }

                    listBox.Items.Add(match.Groups[1].Value);
                }
            }

            return base.ShowDialog();
        }

        bool ValidateName()
        {
            bool isNameAlreadyExisting = false;

            foreach (string item in listBox.Items)
            {
                if (item == textBox.Text)
                {
                    isNameAlreadyExisting = true;
                    break;
                }
            }

            if (!isNameAlreadyExisting)
            {
                return true;
            }

            if (MessageBox.Show(string.Format("{0} already exists. Override it?", textBox.Text), "Override", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                return true;
            }
            return false;
        }

        public string FileName
        {
            get { return textBox.Text; }
        }

        public string FullPath
        {
            get { return Application.StartupPath + @"\files\" + textBox.Text + ".json"; }
        }
    }
}
