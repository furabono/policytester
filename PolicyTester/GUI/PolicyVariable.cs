﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolicyTester.GUI
{
    public partial class PolicyVariable : UserControl
    {
        public PolicyVariable()
        {
            InitializeComponent();

            nameLabel.MouseDoubleClick += NameLabel_MouseDoubleClick;
            nameTextBox.KeyDown += NameTextBox_KeyDown;
            nameTextBox.Leave += NameTextBox_Leave;
            removeButton.Click += RemoveButton_Click;

            valueNUD.Minimum = int.MinValue;
            valueNUD.Maximum = int.MaxValue;

            nameTextBox.HideSelection = true;
            nameTextBox.Text = "Name";
            SetName();
        }

        public PolicyVariable(string name) : this()
        {
            nameTextBox.Text = name;
            SetName();
        }

        public EventHandler RemoveButtonClick;

        private void NameLabel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            nameTextBox.Visible = true;
            nameTextBox.SelectAll();
            nameTextBox.Focus();
        }

        private void NameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetName();
            }
        }

        private void NameTextBox_Leave(object sender, EventArgs e)
        {
            SetName();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            RemoveButtonClick(sender, e);
        }

        private void SetName()
        {
            nameLabel.Text = "<" + nameTextBox.Text + ">";
            nameTextBox.Visible = false;
            //nameTextBox.Hide();
        }

        public string GetName()
        {
            return nameLabel.Text.Substring(1, nameLabel.Text.Length - 2);
        }

        public float GetValue()
        {
            return (float)valueNUD.Value;
        }

        public void EditMode()
        {
            nameTextBox.Visible = true;
            nameTextBox.SelectAll();
            nameTextBox.Focus();
        }
    }
}
