﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace PolicyTester.Utility
{
    class JsonReader
    {
        public static DataTable ReadCitizenData(string filename)
        {
            JObject root;
            using (StreamReader file = File.OpenText(filename))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                root = JObject.Load(reader);
            }

            DataTable dt = new DataTable();
            string[][] qualitatives;
            {
                JArray parameter = (JArray)root["Parameter"];
                qualitatives = new string[parameter.Count][];
                DataColumn dc;
                for (int i = 0; i < parameter.Count; ++i)
                {
                    dc = new DataColumn(parameter[i]["Name"].Value<string>());
                    if (parameter[i]["Type"].Value<string>() == "Qualitative")
                    {
                        dc.DataType = typeof(string);
                        qualitatives[i] = (from category in parameter[i]["Category"]
                                           select category.Value<string>()).ToArray();
                    }
                    else
                    {
                        dc.DataType = typeof(int);
                    }
                    dt.Columns.Add(dc);
                }
            }

            {
                object[] newRow;
                JArray citizen = (JArray)root["Citizen"];
                JArray c;
                for (int i = 0; i < citizen.Count; ++i)
                {
                    c = (JArray)citizen[i];
                    newRow = new object[c.Count];
                    for (int j = 0; j < c.Count; ++j)
                    {
                        if(qualitatives[j] == null)
                        {
                            newRow[j] = c[j].Value<int>();
                        }
                        else
                        {
                            newRow[j] = qualitatives[j][c[j].Value<int>()];
                        }
                    }
                    dt.Rows.Add(newRow);
                }
            }

            return dt;
        }

        public static Task<DataTable> ReadCitizenDataAsync(string filename)
        {
            return Task.Run(() => { return ReadCitizenData(filename); });
        }

        public static Policy ReadPolicyData(string filename)
        {
            JObject root;
            using (StreamReader file = File.OpenText(filename))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                root = JObject.Load(reader);
            }

            string name = root["Name"].Value<string>();
            var variables = (from v in root["variable"]
                             select new KeyValuePair<string, float>(v["Name"].Value<string>(), v["Value"].Value<float>())).ToArray();
            string script = root["Script"].Value<string>();

            return new Policy(name, variables, script);
        }

        public static Task<Policy> ReadPolicyDataAsync(string filename)
        {
            return Task.Run(() => { return ReadPolicyData(filename); });
        }

        public static void WritePolicyData(string filename, string policyName, KeyValuePair<string, float>[] variables, string script)
        {
            JObject root = new JObject();
            root.Add("Name", new JValue(policyName));
            root.Add("Variable", new JArray(from v in variables
                                            select new JObject(new JProperty("Name", v.Key),
                                                               new JProperty("Value", v.Value))
                                ));
            root.Add("Script", new JValue(script));

            using (StreamWriter sw = new StreamWriter(File.OpenWrite(filename)))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                root.WriteTo(writer);
            }
        }
    }
}
