﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PolicyTester.Utility
{
    class Simulator
    {
        public static float[] Simulate(DataTable citizen, KeyValuePair<string, float>[] variables, string script)
        {
            var engine = IronPython.Hosting.Python.CreateEngine();
            var scope = engine.CreateScope();

            scope.SetVariable("citizen", citizen.Select());

            IronPython.Runtime.List var = new IronPython.Runtime.List();
            foreach (KeyValuePair<string, float> v in variables)
            {
                scope.SetVariable(v.Key, v.Value);
                var.append(v.Value);
            }
            scope.SetVariable("var", var);

            string pre_script = "";
            pre_script += "from System import Single" + Environment.NewLine;
            pre_script += "from System import Array" + Environment.NewLine;
            pre_script += Environment.NewLine;
            pre_script += "def Compute(i):" + Environment.NewLine;
            pre_script += "\tparam = citizen[i]" + Environment.NewLine;
            for (int i = 0; i < citizen.Columns.Count; ++i)
            {
                pre_script += string.Format("\t{0} = param[{1}]", citizen.Columns[i].ColumnName.Replace(' ', '_'), i) + Environment.NewLine;
            }
            foreach (string line in script.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
            {
                pre_script += "\t" + line + Environment.NewLine;
            }
            pre_script += "\treturn result" + Environment.NewLine;
            pre_script += Environment.NewLine;
            pre_script += "results = Array.CreateInstance(Single, citizen.Length)" + Environment.NewLine;
            pre_script += "for i in range(0, citizen.Length):" + Environment.NewLine;
            pre_script += "\tresults[i] = Compute(i)" + Environment.NewLine;

            var scriptSource = engine.CreateScriptSourceFromString(pre_script);
            scriptSource.Execute(scope);

            return scope.GetVariable<float[]>("results");
        }

        public static Task<float[]> SimulateAsync(DataTable citizen, KeyValuePair<string, float>[] variables, string script)
        {
            return Task.Run(() => { return Simulate(citizen, variables, script); });
        }

        public static float[] SimulateParallel(DataTable citizen, KeyValuePair<string, float>[] variables, string script)
        {
            var engine = IronPython.Hosting.Python.CreateEngine();
            var scope = engine.CreateScope();

            scope.SetVariable("citizen", citizen.Select());

            IronPython.Runtime.List var = new IronPython.Runtime.List();
            foreach (KeyValuePair<string, float> v in variables)
            {
                scope.SetVariable(v.Key, v.Value);
                var.append(v.Value);
            }
            scope.SetVariable("var", var);

            string pre_script = "";
            pre_script += "from System import Single" + Environment.NewLine;
            pre_script += "from System import Array" + Environment.NewLine;
            pre_script += "from System import Action" + Environment.NewLine;
            pre_script += "from System.Threading.Tasks import *" + Environment.NewLine;
            pre_script += Environment.NewLine;
            pre_script += "results = Array.CreateInstance(Single, citizen.Length)" + Environment.NewLine;
            pre_script += Environment.NewLine;
            pre_script += "def Compute(i):" + Environment.NewLine;
            pre_script += "\tparam = citizen[i]" + Environment.NewLine;
            for (int i = 0; i < citizen.Columns.Count; ++i)
            {
                pre_script += string.Format("\t{0} = param[{1}]", citizen.Columns[i].ColumnName.Replace(' ', '_'), i) + Environment.NewLine;
            }
            foreach (string line in script.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
            {
                pre_script += "\t" + line + Environment.NewLine;
            }
            pre_script += "\tresults[i] = result" + Environment.NewLine;
            pre_script += Environment.NewLine;
            pre_script += "Parallel.For(0, citizen.Length, Action[int](Compute))" + Environment.NewLine;

            var scriptSource = engine.CreateScriptSourceFromString(pre_script);
            scriptSource.Execute(scope);

            return scope.GetVariable<float[]>("results");
        }

        public static Task<float[]> SimulateParallelAsync(DataTable citizen, KeyValuePair<string, float>[] variables, string script)
        {
            return Task.Run(() => { return SimulateParallel(citizen, variables, script); });
        }
    }
}
