﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolicyTester.Utility
{
    class Policy
    {
        public Policy(string name, KeyValuePair<string, float>[] variables, string script)
        {
            this.name = name;
            this.variables = variables;
            this.script = script;
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        KeyValuePair<string, float>[] variables;
        public KeyValuePair<string, float>[] Variables
        {
            get { return variables; }
            set { variables = value; }
        }

        string script;
        public string Script
        {
            get { return script; }
            set { script = value; }
        }
    }
}
